import React, { Component } from "react";
import SimpleStorageContract from "./contracts/CookieStorage.json";
import getWeb3 from "./getWeb3";

import "./App.css";

class App extends Component {
  state = { storageValue: '',storageCount:0, web3: null, accounts: null, contract: null };

  componentDidMount = async () => {
    try {

      const web3 = await getWeb3();

      const accounts = await web3.eth.getAccounts();


      const networkId = await web3.eth.net.getId();
      const deployedNetwork = SimpleStorageContract.networks[networkId];
      const instance = new web3.eth.Contract(
        SimpleStorageContract.abi,
        deployedNetwork && deployedNetwork.address,
      );


      this.setState({ web3, accounts, contract: instance });
    } catch (error) {

      alert(
        `Failed to load web3, accounts, or contract. Check console for details.`,
      );
      console.error(error);
    }
  };

  CookieHandler = async () => {
    const { accounts, contract } = this.state;
const inputValue =document.getElementById('input').value;
    // Stores a given value, 5 by default.
    await contract.methods.setName(inputValue).send({ from: accounts[0] });

    // Get the value from the contract to prove it worked.
    const response = await contract.methods.getName().call();
   
    // Update state with the result.
    this.setState({ StorageValue: response });
  };

  CounterHandler = async () => {
    const { accounts, contract } = this.state;
const inputValue =document.getElementById('input').value;
    // Stores a given value, 5 by default.
    await contract.methods.setCookie(inputValue).send({ from: accounts[0] });

    // Get the value from the contract to prove it worked.
    const response = await contract.methods.getCookie().call();
   
    // Update state with the result.
    this.setState({ StorageCount: response });
  };

  render() {
    if (!this.state.web3) {
      return <div className="welcome">Welcome to Cookie Jar</div>;
    }
    return (
      
      <div className="space">
        
      
      <div className="App1">
        
       
        <p>
          Our Avilable Cookies are  <div>{this.state.storageValue}</div>
        </p>
       
        <p>Set your Cookie Name</p>

        <div style={{flex:1, justifyContent:"center"}}>
        <label>
        Cookie Name
        </label>
        
          <input type="text"
          id="input"/>
          <button type="submit" onClick={()=>this.CookieHandler()}>Submit</button>
        </div>

      </div>
      <div className="App2">
      
       
       <p>
         Our Avilable Cookies Count<div>{this.state.StorageCount}</div> 
       </p>
      
       <p>Set your Cookie Count</p>
        <label>
          Cookie Count:
          <input type="number"
          id="input"/>
          <button type="submit" onClick={()=>this.CounterHandler()}>Submit</button>
        </label>
      </div>
      <div className="App2">
      
       
       <p>
         Our Avilable Cookies Count<div>{this.state.StorageCount}</div> 
       </p>
      
       <p>Set your Cookie Count</p>
        <label>
          Cookie Count:
          <input type="number"
          id="input"/>
          <button type="submit" onClick={()=>this.CounterHandler()}>Submit</button>
        </label>
      </div>
      </div>
    );
  }
}

export default App;
